import axios from 'axios'
import moment from 'moment'
import CryptoJS from 'crypto-js'
import Cookies from 'js-cookie'
import Router from 'next/router'

let isAlreadyFetchingAccessToken = false

const generateSignature = () => {
  const date = moment().format()
  const ciphertext = CryptoJS.AES.encrypt(date, process.env.signatureKey).toString()
  return ciphertext
}

let urls = {
  development: '',
  production: ''
}

const api = axios.create({
  baseURL: urls[process.env.NODE_ENV],
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
})

api.interceptors.request.use(function (config) {
  config.headers.signature = generateSignature()

  return config
})

api.interceptors.response.use(function (response) {
  return response
}, async function (error) {
  const { config, response: { status } } = error
  const originalRequest = config

  if (status === 401) {
    if (!isAlreadyFetchingAccessToken) {
      isAlreadyFetchingAccessToken = true

      const refreshToken = Cookies.get('refreshToken')
      const username = Cookies.get('username')
      try {
        const { data: { token } } = await api.post('/api/v1/user/token', { username, refreshToken })
        const retryOriginalRequest = new Promise((resolve) => {
          api.defaults.headers.Authorization = `Bearer ${token}`
          Cookies.set('token', token)
          originalRequest.headers.Authorization = `Bearer ${token}`
          resolve(axios(originalRequest))
        })

        isAlreadyFetchingAccessToken = false
        return retryOriginalRequest
      } catch (err) {
        isAlreadyFetchingAccessToken = false
        Router.push('/login')
      }
    }
  }
  return Promise.reject(error)
})

export default api