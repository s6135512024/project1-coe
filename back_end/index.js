const express = require("express")
const cors = require('cors')
const admin = require("firebase-admin");
const { getAuth } = require('firebase-admin/auth');

const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const app = express()
app.use(cors())

// const { initializeApp } = require('firebase-admin/app');

// const firebase = initializeApp({
//     apiKey: "AIzaSyB-1eDs_7wYj-53kyV1cApYD2hoVtXIqUw",
//     authDomain: "digitalcerti-pj.firebaseapp.com",
//     projectId: "digitalcerti-pj",
//     storageBucket: "digitalcerti-pj.appspot.com",
//     messagingSenderId: "243298527795",
//     appId: "1:243298527795:web:f95f2a7e43c02531bf5146",
//     measurementId: "G-GLPHMCVHHN"
//   });

app.get("/",async (req ,res)=>{
    const users = await getAuth().listUsers(100)

    res.send(users)
})

app.listen(3001 , ()=>{
    console.log(3001)
})
