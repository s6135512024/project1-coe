import React, { useState, useEffect } from 'react'
import { useUpdate } from "react-use"
import { Layout, Menu, Popconfirm } from 'antd'
import Link from 'next/link'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  FileTextOutlined,
  UserOutlined,
  IssuesCloseOutlined,
  LogoutOutlined,
  UploadOutlined 
} from '@ant-design/icons'

const { Header, Sider, Content } = Layout
import { useAuth } from '../contexts/auth'
let mounted = false


const LayoutComponent  = ({ children }) => {
  const [collapsed, toggleCollapsed] = useState(false)
  const authContext = useAuth()
  const update = useUpdate()
  useEffect(() => {
    if (mounted == false) {
      update()
      mounted = true
    }
  }, [update])

  const handleToggle = () => {
    toggleCollapsed(!collapsed)
  }

  const handleConfirmLogout = () => {
    authContext['logout']()
  }

  return (
    <>
      {
        mounted && <Layout id="layout-component">
          <Sider trigger={null} collapsible collapsed={collapsed}>
            {
              !collapsed && <div className="logo-container">
                <div className="logo-title">DGC DIGICERTY</div>
              </div>
            }
            <Menu theme="light" mode="inline" defaultSelectedKeys={['1']} className="menu-container">
              <Menu.Item key="1" icon={<UploadOutlined  />}>
                <Link href={`/`}>
                  <a className="menu-title">Upload</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<FileTextOutlined  />}>
                <Link href={`/user`}>
                  <a className="menu-title">File</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="3" icon={<UserOutlined />}>
                <Link href={`/problem`}>
                  <a className="menu-title">User</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="4" icon={<IssuesCloseOutlined />}>
                <Link href={`/order`}>
                  <a className="menu-title">Problem</a>
                </Link>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Header className="site-layout-background" style={{ padding: 0 }}>
              <div className="header-container">
                {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                  className: 'trigger',
                  onClick: handleToggle
                })}
                <Popconfirm
                  placement="bottomRight"
                  title={'Are you sure to logout?'}
                  onConfirm={handleConfirmLogout}
                  okText="Yes"
                  cancelText="No"
                >
                  <LogoutOutlined className="logout" />
                </Popconfirm>
              </div>
            </Header>
            <Content
              className="site-layout-background"
              style={{
                margin: '24px 16px',
                padding: 24,
                minHeight: 280,
              }}
            >
              {children}
            </Content>
          </Layout>
        </Layout>
      }

      <style jsx>{`
        .logo-container {
          height: 64px;
          margin: 16px;
          background: #fff;
          border-radius: 10px;
          display: flex;
          align-items: center;
        }

        .logo-title {
          font-family: Poppins-Bold;
          font-size: 24px;
          color: #333333;
          line-height: 1.2;
          text-align: center;
        }

        .header-container {
          display: flex;
          justify-content: space-between;
          align-items: center;
        }

        .menu-title {
          color: inherit;
        }
      `}</style>

      <style jsx global>{`
        #layout-component .logout {
          padding: 0 24px;
          font-size: 18px;
          line-height: 64px;
          cursor: pointer;
          transition: color 0.3s;
        }

        #layout-component .trigger {
          padding: 0 24px;
          font-size: 18px;
          line-height: 64px;
          cursor: pointer;
          transition: color 0.3s;
        }
        
        #layout-component .trigger:hover {
          color: #1890ff;
        }
        
        #layout-component .site-layout .site-layout-background {
          background: #fff;
        }

        #layout-component {
          height: 100vh;
        }

        #layout-component .ant-layout-sider-children {
          background-color: #5f5cff;
        }

        #layout-component .menu-container {
          background-color: #5f5cff;
          color: #fff;
        }

        #layout-component .ant-menu-inline {
          width: 100.5%;
        }
      `}</style>
    </>
  )
}

export default LayoutComponent