import React, { createContext, useState, useContext, useEffect } from 'react'
import Cookies from 'js-cookie'
import Router, { useRouter } from 'next/router'
import Login from '../pages/login'
import api from '../services/api'

const AuthContext = createContext({})

export const AuthProvider = ({ children }) => {

  const [user, setUser] = useState(null)
  const [isLoading, setLoading] = useState(true)

  useEffect(() => {
    async function loadUserFromCookies() {
      const token = Cookies.get('token')
      if (token) {
        api.defaults.headers.Authorization = `Bearer ${token}`
        try {
          const { data: { result } } = await api.get('/api/v1/user/profile')
          if (result) setUser(result)
        } catch (error) { }
      }
      setLoading(false)
    }
    loadUserFromCookies()
  }, [])

  const login = async (username, password) => {
    try {
      const { data: { token, refreshToken } } = await api.post('/api/v1/user/login', { username, password })
      if (token) {
        Cookies.set('token', token)
        Cookies.set('refreshToken', refreshToken)
        api.defaults.headers.Authorization = `Bearer ${token}`
        const { data: { result } } = await api.get('/api/v1/user/profile')
        Cookies.set('username', result.username)
        setUser(result)
        Router.push('/')
        return {}
      }
    } catch (error) {
      return { message: 'username or password is wrong.' }
    }
  }

  const logout = () => {
    Cookies.remove('token')
    Cookies.remove('refreshToken')
    Cookies.remove('username')
    setUser(null)
    delete api.defaults.headers.Authorization
    return Router.push('/login')
  }

  return (
    // <AuthContext.Provider value={{ isAuthenticated: !!user, user, login, logout, isLoading }}>
  <AuthContext.Provider value={{ isAuthenticated:true, user, login, logout, isLoading }}>
      {children}
    </AuthContext.Provider>
  )
}

export const useAuth = () => useContext(AuthContext)

export const ProtectRoute = ({ children }) => {
  const router = useRouter()
  const { isAuthenticated, isLoading } = useAuth()

  if (!isLoading && !isAuthenticated && router.route !== '/login') {
    return <Login />
  }
  return children
}