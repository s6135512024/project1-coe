import { Upload, message, Button } from "antd";
import React, { useState } from "react";
import {
  UserOutlined,
  SolutionOutlined,
  LoadingOutlined,
  SmileOutlined,
  InboxOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { getDownloadURL, ref, uploadBytesResumable } from "@firebase/storage";
import { storage } from "./firebase";
import {getFirestore, collection, addDoc, doc, updateDoc, arrayUnion, setDoc, getDoc} from 'firebase/firestore'




const { Dragger } = Upload;

const Dashboard = ({ }) => {
  const [file, setFile] = useState(null);
  const [fileURL, setURL] = useState(null);
  const props = {
    name: "file",
    multiple: true,
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
      }
      if (status === "done") {
        setFile(info.file);
        console.log(info.file)
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
  };
  const validateMessages = {
    required: "${label} is required!",
    types: {
      email: "${label} is not a valid email!",
      number: "${label} is not a valid number!",
    },
    number: {
      range: "${label} must be between ${min} and ${max}",
    },
  };

  const handleUpload = () => {
    if (file !== null) {
      const typefile = file.originFileObj.type.split('/')
      const refPath = `file/${new Date().getTime()}.${typefile[typefile.length - 1]}.pdf`
      const storageRef = ref(storage, refPath);
      // console.log("refPath", refPath);
      // setURL(refPath)
      const uploadTask = uploadBytesResumable(storageRef, file.originFileObj);
      uploadTask.on(
        "state_changed",
        (snapshot) => { },
        (err) => {
          console.log(err);
        },
        async () => {
          const url = await getDownloadURL(ref(storage, refPath))
          setURL(url)
          const db = getFirestore()
          // await addDoc(collection, 'userfiles'), {
          //   url
          // })
          // await addDoc(collection(db, "userfiles"), {
          //   url: refPath
          // })
          // updateDoc(doc(db, "userfiles", localStorage.getItem('uid')), {
          //   urls: arrayUnion(refPath)
          // })
          const userRef = doc(db, "userfiles", localStorage.getItem('uid'))
          const docSnap = await getDoc(userRef)
          if(docSnap.exists()) {
            await updateDoc(userRef, {
              urls: [
                ...docSnap.data().urls,
                refPath
              ] 
            })
          } else {
            await setDoc(userRef, {
              urls: [
                refPath
              ] 
            })
          }
          
          // await db.collection("userfiles").doc(localStorage.getItem('uid')).set()
          console.log('++++++++++++');
          // console.log("url ", url);
          // getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          //   setURL(url)
          //   console.log(url); 
          // });
        }
      );
    }
  };

  return (
    <div>
      {fileURL ?
        <iframe src={fileURL} width={600} height={700} >

        </iframe> : null
      }
      <Dragger {...props}>
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <p className="ant-upload-text">
          Click or drag file to this area to upload
        </p>
        <p>Or</p>
        <Button icon={<UploadOutlined />}>Select File</Button>
      </Dragger>

      <Button
        type="primary"
        onClick={handleUpload}
        //disabled={fileList.length === 0}
        // loading={uploading}

        style={{ marginTop: 16 }}
      >
        Upload
      </Button>
      <br></br>
      <br></br>
      <button type="submit" className='btn btn-success btn-lg'
        style={{ marginTop: 16 }}
      >



      </button>

    </div>
  );
};


export default Dashboard;
