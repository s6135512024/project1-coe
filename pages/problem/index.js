import React, { useEffect, useState } from 'react'
// Import the main component
import { Viewer } from '@react-pdf-viewer/core'; // install this library
// Plugins
import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout'; // install this library
// Import the styles
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';
// Worker
import { Worker } from '@react-pdf-viewer/core'; // install this library

import axios from 'axios'
import { doc, getDoc, getFirestore } from 'firebase/firestore';
import { getDownloadURL, getStorage, ref } from 'firebase/storage';

// export const App = () => {

//   // Create new plugin instance
//   const defaultLayoutPluginInstance = defaultLayoutPlugin();

//   // for onchange event
//   const [pdfFile, setPdfFile]=useState(null);
//   const [pdfFileError, setPdfFileError]=useState('');

//   // for submit event
//   const [viewPdf, setViewPdf]=useState(null);

//   // onchange event
//   const fileType=['application/pdf'];
//   const handlePdfFileChange=(e)=>{
//     let selectedFile=e.target.files[0];
//     if(selectedFile){
//       if(selectedFile&&fileType.includes(selectedFile.type)){
//         let reader = new FileReader();
//             reader.readAsDataURL(selectedFile);
//             reader.onloadend = (e) =>{
//               setPdfFile(e.target.result);
//               setPdfFileError('');
//             }
//       }
//       else{
//         setPdfFile(null);
//         setPdfFileError('Please select valid pdf file');
//       }
//     }
//     else{
//       console.log('select your file');
//     }
//   }

//   // form submit
//   const handlePdfFileSubmit=(e)=>{
//     e.preventDefault();
//     if(pdfFile!==null){
//       setViewPdf(pdfFile);
//     }
//     else{
//       setViewPdf(null);
//     }
//   }

//   return (
//     <div className='container'>

//     <br></br>

//       <form className='form-group' onSubmit={handlePdfFileSubmit}>
//         <input type="file" className='form-control'
//           required onChange={handlePdfFileChange}
//         />
//         {pdfFileError&&<div className='error-msg'>{pdfFileError}</div>}
//         <br></br>
//         <button type="submit" className='btn btn-success btn-lg'
//           style={{ marginTop: 16 }}
//         >

//           View PDF

//         </button>

//       </form>
//       <br></br>
//       <div className='pdf-container'>
//         {/* show pdf conditionally (if we have one)  */}
//         {viewPdf&&<><Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
//           <Viewer fileUrl={viewPdf}
//             plugins={[defaultLayoutPluginInstance]} />
//       </Worker></>}

//       {/* if we dont have pdf or viewPdf state is null */}
//       {!viewPdf&&<>No pdf file selected</>}
//       </div>

//     </div>
//   )
// }

// export default App



// const Problem = ({ }) => {
//   return (
//     <div> ติดต่อ  Tell : 0980529795ghghjghy<br></br><br></br>Facebook : Pd pandora</div>
//   )
// }

// export default Problem

const App = () => {

  const [usersData, setUsersData] = useState([])

  useEffect(() => {
    whenInit()
  }, [])

  const whenInit = async () => {
    //  const res =  await getAuth().
    //  console.log(res);
    const {
      data: {
        users
      }
    } = await axios.get('http://localhost:3001')
    const _usersData = await Promise.all(users.map(async (user) => {
      const { providerData } = user
      const _provider = providerData[0]
      const db = getFirestore()
      const userRef = doc(db, "userfiles", user.uid)
      const docSnap = await getDoc(userRef)
      if (docSnap.exists()) {
        const storage = getStorage()
        const donwloadUrls = await Promise.all(
          docSnap.data().urls.map(async (url) => {
            const downloadUrl = await getDownloadURL(ref(storage, url))
            return { downloadUrl, url }
          })
        )
        return {
          email: _provider.email,
          providerId: _provider.providerId,
          uid: user.uid,
          urls: donwloadUrls
        }
      } else {
        return {
          email: _provider.email,
          providerId: _provider.providerId,
          uid: user.uid,
          urls: []
        }
      }

    }))
    setUsersData(_usersData)
  }

  return (

    <table>
      <tr>
        <th>type</th>
        <th>email</th>
        <th>files</th>
      </tr>
      {
        usersData.map((user) => {
          return (
            <tr>
              <td>{user.email}</td>
              <td>{user.providerId}</td>
              <td>
                {
                  user.urls.map((url) => {
                    return <div>
                      <a target="_blank" href={url.downloadUrl}  >{url.url.split("/")[1]}</a>&nbsp;</div>
                  })
                }
              </td>
            </tr>
          )
        })
      }
      {/* <tr>
        <td>Alfreds Futterkiste</td>
        <td>Maria Anders</td>
        <td>Germany</td>
      </tr>
      <tr>
        <td>Centro comercial Moctezuma</td>
        <td>Francisco Chang</td>
        <td>Mexico</td>
      </tr>
      <tr>
        <td>Ernst Handel</td>
        <td>Roland Mendel</td> 
        <td>Austria</td>
      </tr>
      <tr>
        <td>Island Trading</td>
        <td>Helen Bennett</td>
        <td>UK</td>
      </tr>
      <tr>
        <td>Laughing Bacchus Winecellars</td>
        <td>Yoshi Tannamuri</td>
        <td>Canada</td>
      </tr>
      <tr>
        <td>Magazzini Alimentari Riuniti</td>
        <td>Giovanni Rovelli</td>
        <td>Italy</td>
      </tr> */}
    </table>
  )

}



export default App