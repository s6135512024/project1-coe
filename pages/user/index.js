import { doc, getDoc, getFirestore } from "firebase/firestore"
import { useEffect, useState } from "react"
import { getDownloadURL, getStorage, ref } from 'firebase/storage'

const Users = ({ }) => {

  const [urls, setUrls] = useState([])

  useEffect(() => {
    whenInit()
  }, [])

  const whenInit = async () => {
    const db = getFirestore()
    const userRef = doc(db, "userfiles", localStorage.getItem('uid'))
    const docSnap = await getDoc(userRef)
    if(docSnap.exists()) {

      const storage = getStorage()
      const donwloadUrls = await Promise.all(
        docSnap.data().urls.map(async (url) => {
          const downloadUrl = await getDownloadURL(ref(storage, url))
          return {downloadUrl,url}
        })
      )
      setUrls(donwloadUrls)
    }
  }

  return (
    <div>แนะนำ <br></br><br></br>ผู้ใช้สามารถดูตัวอย่างไฟล์ได้ในหน้านี้<br/>
    {urls.map(({downloadUrl,url}) => {
      return <div><a target="_blank" href={downloadUrl}  >{url.split("/")[1]}</a></div>
    })}
    </div>
  )
}

export default Users