import firebase from 'firebase/compat/app'
import 'firebase/compat/auth'
import { initializeApp } from "firebase/app";
import { getStorage,ref } from "firebase/storage";


const firebaseConfig = {
  apiKey: "AIzaSyB-1eDs_7wYj-53kyV1cApYD2hoVtXIqUw",
  authDomain: "digitalcerti-pj.firebaseapp.com",
  projectId: "digitalcerti-pj",
  storageBucket: "digitalcerti-pj.appspot.com",
  messagingSenderId: "243298527795",
  appId: "1:243298527795:web:f95f2a7e43c02531bf5146",
  measurementId: "G-GLPHMCVHHN"
};
  // firebase.initializeApp(firebaseConfig);
  
  // const storage = getStorage(firebaseApp);
  // const storageRef = ref(storage);
  // // Get a reference to the storage service
// export { storage,storageRef,firebaseApp };

if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

export  const  app =  initializeApp(firebaseConfig);
export  const  storage = getStorage(app);
export  default firebase.auth() 