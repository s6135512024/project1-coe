import verifyPDF from '@ninja-labs/verify-pdf';

const readFile = (e) => {
    const file = e.target.files[0]
    let reader = new FileReader();
    reader.onload = function(e) {
        const { verified } = verifyPDF(reader.result);
    }
    reader.readAsArrayBuffer(file);
};